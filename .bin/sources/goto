#!/bin/sh
# A posix friendly rewrite of the amazing goto command by iridakos

__goto_get_db() {
    # TODO: Respect if the variable is already set
    __GOTO_DB="$HOME/.gotodb"
    touch -a "$__GOTO_DB"
}

# Simple error wrapper
__goto_error() {
    (>&2 printf 'goto error: %s\n' "$1")
}

# Simple function that helps expand things like ~
__goto_resolve_directory() {
    builtin cd "$1" 2>/dev/null && pwd
}

# Returns the directory of a given alias
__goto_find_alias_directory() {
    sed -n "s/^$1 \\(.*\\)/\\1/p" "$__GOTO_DB" 2> /dev/null
}

# Returns the directory of a given alias or errors out
__goto_resolve_alias() {
    resolved=$(__goto_find_alias_directory "$1")

    if [ -z "$resolved" ]; then
        __goto_error "alias '$1' doesn't exist"
        return 1
    else
        echo "${resolved}"
    fi
}

# Checks for duplicate entries in gotodb
__goto_find_duplicates() {
    sed -n 's:[^ ]* '"$1"'$:&:p' "$__GOTO_DB" 2> /dev/null
}

# Performs the actual goto
__goto_goto() {
    target=$(__goto_resolve_alias "$1") || return 1

    builtin cd "$target" 2> /dev/null || \
        { __goto_error "Failed to goto '$target'" && return 1; }
}

# Adds an entry into the gotodb
__goto_add() {
    # Check if argument count is correct
    if [ "$#" -ne "2" ]; then
        __goto_error "usage: goto --add <alias> <path>"
        return 1
    fi

    # Check if the alias is valid
    if ! echo "$1" | grep -Eq "^[a-zA-Z0-9]*[a-zA-Z0-9]+$"; then
        __goto_error "$1 is an invalid alias name, aliases can only start with a letter
        or a number followed by letters, numbers hyphens or underscores"
        return 1
    fi

    # Check if the directory exists
    directory=$(__goto_resolve_directory "$2")
    if [ -z "$directory" ]; then
        __goto_error "path not found: $2"
    fi

    # Check if the current entry already exists
    duplicates=$(__goto_find_duplicates "$1")
    if [ -n "$duplicates" ]; then
        __goto_error "entry already exists: $duplicates"
        return 1
    fi
    echo $duplicates

    # Append to gotodb
    echo "$1 $directory" >> "$__GOTO_DB"
    echo "registered alias $1 succesfully"
}

# Main command
goto() {
    __goto_get_db

    if [ -z "$1" ]; then
        __goto_help
        return
    fi

    subcommand="$1"
    shift

    case "$subcommand" in
        -a|--add)
            __goto_add "$@"
            ;;
        -d|--delete)
            __goto_delete "$@"
            ;;
        -l|--list)
            __goto_list "$@"
            ;;
        *)
            __goto_goto "$subcommand"
            ;;
    esac

    return $?
}
