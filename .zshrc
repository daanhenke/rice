# Export all variables in ~/.env
source ~/.env
export $(cut -d= -f1 ~/.env)

# Autosource all files in ~/.bin/sources
for f in ~/.bin/sources/*; do source $f; done
