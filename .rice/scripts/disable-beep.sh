#!/usr/bin/env sh
echo "Making sure pcspkr doesn't get loaded on boot"
echo "blacklist pcspkr" | sudo tee -a /etc/modprobe.d/nobeep.conf > /dev/null
echo "Disabling pcspkr for the current session"
sudo rmmod pcspkr 2> /dev/null
