" pre plugin configuration
    " disable vi compatibility mode
    set nocompatible

    " makes plugins work better (eledgedly)
    filetype off

" plugin loading
call plug#begin('~/.vim/plugged')
    Plug 'yggdroot/indentline'
    Plug 'ntk148v/vim-horizon'
    Plug 'baskerville/vim-sxhkdrc'
call plug#end()

" general configuration after plugin loading
    " enable syntax highlighting
    syntax on

    " enable autio indentation
    filetype plugin indent on

    " enable line numbers
    set number

    " show file info
    set ruler

    " set default encoding
    set encoding=utf-8

    " whitespace configuration
    set tabstop=4
    set shiftwidth=4
    set expandtab
    set wrap
    set softtabstop=4
    set noshiftround
    set textwidth=79

" theming
    set t_Co=256
    set t_ut=
    set background=dark
    " set colorscheme
    colorscheme horizon
    " remove background since this is handled by the terminal emulator
    highlight Normal        ctermbg=NONE    guibg=NONE
    highlight SignColumn    ctermbg=NONE    guibg=NONE 
    highlight LineNr        ctermbg=NONE    guibg=NONE 
    highlight NonText       ctermbg=NONE    guibg=NONE
    highlight EndOfBuffer   ctermbg=NONE    guibg=NONE

" plugin configuration
    " indentline
    let g:indentLine_char = '┊'
